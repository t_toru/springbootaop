package com.example.demo;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class HelloAop {
    @Before("execution(public * com.example.demo..*.*(..))")
    public void before(JoinPoint joinpoint) {
        System.out.println("MethodStart: " + joinpoint.getSignature());
    }
    @After("execution(public * com.example.demo..*.*(..))")
    public void after(JoinPoint joinpoint) {
        System.out.println("MethodEnd: " + joinpoint.getSignature());
    }

}
