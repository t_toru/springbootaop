package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    // AOP対象にしたいクラスを@Autowiredで自動インスタンス生成
    @Autowired
    HelloUtil util;
    
    // AOP対象になる
    @RequestMapping("/")
    public String index() {
        return util.getIndex();
    }
    
    // AOP対象になる
    @RequestMapping("/test")
    public String test() {
        return util.getTest();
    }

    // AOP対象になる
    @RequestMapping("/test2")
    public String test2() {
        return util.getTest2();
    }

    // AOP対象になる
    @RequestMapping("/test3")
    public String test3() {
        return util.getTest3();
    }
}
