package com.example.demo;

import org.springframework.stereotype.Component;

// @Componentを記載して使用元で@Autowiredを記載しないとAOP対象にならない
@Component
public class HelloUtil {
    // AOP対象になる
    public String getIndex() {
        return "**getIndex";
    }
    
    // AOP対象になる
    public String getTest() {
        return "**getTest -> " + this.getTestProtected();
    }
    
    // protectedだとAOP対象にならない
    protected String getTestProtected() {
        return "**getTestProtected";
    }

    // AOP対象になる
    public String getTest2() {
        // protectedだとAOP対象にならない
        return "**getTest2 -> " + this.getTest();
    }
    
    // AOP対象になる
    public String getTest3() {
        // AOP対象になる
        return "**getTest3 -> " + this.getTest2();
    }

}
